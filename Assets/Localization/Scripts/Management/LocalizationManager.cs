﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections.Generic;
using UnityEngine;
using Wild.Freelance.Localization.Data;

namespace Wild.Freelance.Localization.Management
{
    public static class LocalizationManager
    {
        static LocalizationManager() { }

        private static LocalizationData _data = Resources.Load<LocalizationData>("WildFreelance/LocalizationData");
        private static List<Phrase> Phrases { get { return _data.phrases; } }

        private const string _languageKeyName = "Wild.Freelance.Localization.CurrentLanguage";
        private static LanguageTypes _language = LoadLanguage();
        private static LanguageTypes LoadLanguage()
        {
            LanguageTypes language = LanguageTypes.English;
            try
            {
                if (PlayerPrefs.HasKey(_languageKeyName))
                    language = (LanguageTypes)PlayerPrefs.GetInt(_languageKeyName);
                else
                    language = GetCalculatedLanguage();
            }
            catch
            {
                language = GetCalculatedLanguage();
            }
            return language;
        }

        private static LanguageTypes GetCalculatedLanguage()
        {
            LanguageTypes language = LanguageTypes.English;
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Russian:
                    language = LanguageTypes.Russian;
                    break;
                case SystemLanguage.Ukrainian:
                    language = LanguageTypes.Russian;
                    break;
                case SystemLanguage.Belarusian:
                    language = LanguageTypes.Russian;
                    break;
                default:
                    language = LanguageTypes.English;
                    break;
            }
            return language;
        }

        public static LanguageTypes Language
        {
            get { return _language; }
            set
            {
                _language = value;
                PlayerPrefs.SetInt(_languageKeyName, (int)_language);
                if(OnLanguageChanged != null)
                    OnLanguageChanged();
            }
        }
        public static event Action OnLanguageChanged;
   
        public static string GetText(LanguageTypes lang, uint phraseId)
        {
            int phraseIndex = (int)phraseId;
            if(Phrases.Count < phraseIndex)
            {
                string exceptionMessage = "phrase id must be less than the total number of phrases";
                Debug.LogException(new Exception(exceptionMessage));
                return exceptionMessage;
            }

            return _data.phrases[phraseIndex].GetText(lang);
        }

        public static string GetText(uint phraseId)
        {
            return GetText(Language, phraseId);
        }
    }
}
