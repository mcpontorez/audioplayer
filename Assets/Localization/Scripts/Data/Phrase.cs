﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using Wild.Freelance.Localization.Management;

namespace Wild.Freelance.Localization.Data
{
    [Serializable]
    public class Phrase
    {
#if UNITY_EDITOR
        [SerializeField, HideInInspector]
        private string _forInspectorId = string.Empty;
#endif
        [SerializeField, HideInInspector]
        private uint _id = 0;
        public uint Id
        {
            get { return _id; }
            set
            {
                _id = value;
#if UNITY_EDITOR
                _forInspectorId = "ID " + _id.ToString();
#endif
            }
        }
        [TextArea(0, 3)]
        public string russian = string.Empty;
        [TextArea(0, 3)]
        public string english = string.Empty;

        public string GetText(LanguageTypes languageType)
        {
            string text = string.Empty;
            switch (languageType)
            {
                case LanguageTypes.English:
                    text = english;
                    break;
                case LanguageTypes.Russian:
                    text = russian;
                    break;
                default:
                    break;
            }
            return text;
        }
    }
}
