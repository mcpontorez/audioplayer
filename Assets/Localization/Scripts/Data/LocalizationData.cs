﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.Localization.Data
{
    [CreateAssetMenu(fileName = "LocalizationData", menuName = "WildFreelance/LocalizationData")]
    public class LocalizationData : ScriptableObject
    {
        public List<Phrase> phrases = new List<Phrase>();

        private void OnValidate()
        {
            for (int i = 0; i < phrases.Count; i++)
            {
                phrases[i].Id = (uint)i;
            }
        }
    }
}
