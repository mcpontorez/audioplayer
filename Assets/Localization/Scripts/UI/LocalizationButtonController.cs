﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Wild.Freelance.Localization.Management;

namespace Wild.Freelance.Localization.UI
{
    [RequireComponent(typeof(Button))]
    public class LocalizationButtonController : MonoBehaviour, IPointerClickHandler
    {
        private Button _button;

        public Sprite russianFlag;
        public Sprite englishFlag;

        private void Awake()
        {
            _button = GetComponent<Button>();
        }

        private void Start()
        {
            SwithSprite();
            LocalizationManager.OnLanguageChanged += SwithSprite;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            SwithLanguage();
        }

        private void SwithLanguage()
        {
            switch (LocalizationManager.Language)
            {
                case LanguageTypes.English:
                    LocalizationManager.Language = LanguageTypes.Russian;
                    break;
                case LanguageTypes.Russian:
                    LocalizationManager.Language = LanguageTypes.English;
                    break;
                default:
                    break;
            }
        }

        private void SwithSprite()
        {
            switch (LocalizationManager.Language)
            {
                case LanguageTypes.English:
                    _button.image.sprite = englishFlag; 
                    break;
                case LanguageTypes.Russian:
                    _button.image.sprite = russianFlag;
                    break;
                default:
                    break;
            }
        }

        private void OnDestroy()
        {
            LocalizationManager.OnLanguageChanged -= SwithSprite;
        }
    }
}
