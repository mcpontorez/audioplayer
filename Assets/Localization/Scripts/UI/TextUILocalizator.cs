﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.Localization.Management;

namespace Wild.Freelance.Localization.UI
{
    [RequireComponent(typeof(Text))]
    public class TextUILocalizator : MonoBehaviour
    {
        private Text _text;
        private Text Text
        {
            get
            {
                if (!_text)
                    _text = GetComponent<Text>();
                return _text;
            }
        }

        [SerializeField]
        private uint _phraseId;
        public uint PhraseId
        {
            get { return _phraseId; }
            set
            {
                _phraseId = value;
                Localizate();
            }
        }

        [SerializeField]
        private string _additionalString;
        public string AdditionalString
        {
            get { return _additionalString; }
            set
            {
                _additionalString = value;
                Localizate();
            }
        }

        private void Start()
        {
            LocalizationManager.OnLanguageChanged += Localizate;
            Localizate();
        }

        private void Localizate()
        {
            string text = LocalizationManager.GetText(_phraseId);
            if (!string.IsNullOrEmpty(_additionalString))
                text += _additionalString;
            Text.text = text;
        }

        private void OnDestroy()
        {
            LocalizationManager.OnLanguageChanged -= Localizate;
        }
    }
}
