﻿//// powered by https://vk.com/mcpontorez

using System;

namespace Wild.Freelance.DiaryManagement
{
    [Serializable]
    public class NoteData
    {
        public uint titlePhraseId;
        public uint descriptionPhraseId;
    }
}
