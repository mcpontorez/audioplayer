﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.Localization.UI;

namespace Wild.Freelance.DiaryManagement
{
    public class NoteUIController : MonoBehaviour
    {
        [SerializeField]
        private TextUILocalizator _title;
        public TextUILocalizator Title { get { return _title; } }

        [SerializeField]
        private TextUILocalizator _description;
        public TextUILocalizator Description { get { return _description; } }

        private NoteData _note;
        public NoteData Note
        {
            get { return _note; }
            set
            {
                _note = value;
                Title.PhraseId = _note.titlePhraseId;
                Description.PhraseId = _note.descriptionPhraseId;
            }
        }
    }
}
