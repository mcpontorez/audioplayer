﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.DiaryManagement
{
    public class DiaryController : MonoBehaviour
    {
        [SerializeField]
        private DiaryCanvasController _diaryCanvasControllerPrefab;
        public DiaryCanvasController DiaryCanvasController { get; private set; }

        private void Awake()
        {
            DiaryCanvasController = Instantiate(_diaryCanvasControllerPrefab);
        }

        public string notesTag = "DiaryNotes";

        public KeyCode InfoButton = KeyCode.Tab;

        private void Update()
        {
            if (Input.GetKeyDown(InfoButton))
                DiaryCanvasController.ShowOrHideInfo();
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            if (otherCollider.tag != notesTag)
                return;

            NoteController noteController = otherCollider.GetComponent<NoteController>();
            if (!noteController)
                return;

            switch (noteController.NoteType)
            {
                case NoteTypes.Usual:
                    DiaryCanvasController.AddNote(noteController.note);
                    break;
                case NoteTypes.Key:
                    DiaryCanvasController.SetKey(noteController.note);
                    break;
            }
            
            Destroy(noteController.gameObject);
        }
    }
}
