﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.Localization.Management;

namespace Wild.Freelance.DiaryManagement
{
    [RequireComponent(typeof(AudioSource))]
    public sealed class DiaryCanvasController : MonoBehaviour
    {
        [HideInInspector]
        [SerializeField]
        private AudioSource _audioSource;

        [SerializeField]
        private GameObject _messageParent;
        [SerializeField]
        private Text _messageText;
        public uint startMessageTextPhraseId = 0;
        [SerializeField]
        private AudioClip _messageAudio;
        [SerializeField]
        [Range(0.5f, 600f)]
        private float _delayMessageHide = 4f;

        [SerializeField]
        private GameObject _infoParent;
        [SerializeField]
        private RectTransform _notesPanel;
        [SerializeField]
        private NoteUIController _noteItemPanel;

        [SerializeField]
        private NoteUIController _keyPanel;

        private NoteData _note;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            ShowMessage(false);
            ShowInfo(false);
        }

        public void AddNote(NoteData note)
        {
            _note = note;
            
            ShowHide(true);
            ShowMessage(true, _note.titlePhraseId);

            NoteUIController noteUI = Instantiate(_noteItemPanel, _notesPanel, false);
            noteUI.Note = _note;       
        }

        public void SetKey(NoteData note)
        {
            _note = note;

            ShowMessage(true, _note.titlePhraseId);

            _keyPanel.gameObject.SetActive(true);
            _keyPanel.Description.PhraseId = _note.descriptionPhraseId;
        }

        private void ShowHide(bool isMessageShowed)
        {
            ShowMessage(isMessageShowed);
            ShowInfo(!isMessageShowed);
        }

        private void ShowMessage(bool isShow = true, uint? messageTextPhraseId = null)
        {
            _messageParent.SetActive(isShow);

            if (messageTextPhraseId == null || !isShow)
                return;

            _messageText.text = LocalizationManager.GetText(startMessageTextPhraseId) + LocalizationManager.GetText((uint)messageTextPhraseId);
            _audioSource.PlayOneShot(_messageAudio);

            if (_hidingMessage != null)
                StopCoroutine(_hidingMessage);
            _hidingMessage = StartCoroutine(HidingMessage());
        }

        public void ShowOrHideInfo()
        {
            if (!_infoParent.activeInHierarchy && _note != null)
                ShowHide(false);
            else
                ShowInfo(false);
        }

        private void ShowInfo (bool isShow = true)
        {
            _infoParent.SetActive(isShow);
        }

        private Coroutine _hidingMessage;
        private IEnumerator HidingMessage()
        {
            yield return new WaitForSeconds(_delayMessageHide);
            ShowMessage(false);
        }
    }
}
