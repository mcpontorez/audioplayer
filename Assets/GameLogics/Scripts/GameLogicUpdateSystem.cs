﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections;
using UnityEngine;
using Wild.Freelance.Singletons;

namespace Wild.Freelance.GameLogics
{
    public class GameLogicUpdateSystem : SingletonMB<GameLogicUpdateSystem>
    {
        public event Action OnUpdated;
        public event Action OnFixedUpdated;
        public event Action OnLateUpdated;

        private void Update()
        {
            if (OnUpdated != null)
                OnUpdated.Invoke();
        }

        private void LateUpdate()
        {
            if (OnLateUpdated != null) 
                OnLateUpdated.Invoke();
        }

        private void FixedUpdate()
        {
            if (OnFixedUpdated != null)
                OnFixedUpdated.Invoke();
        }

        public void InvokeWithDelay(float delay, Action action)
        {
            StartCoroutine(Invoking(delay, false, action));
        }

        public void InvokeWithDelayRealtime(float delay, Action action)
        {
            StartCoroutine(Invoking(delay, true, action));
        }

        private IEnumerator Invoking(float delay, bool isRealtime, Action action)
        {
            if (isRealtime)
                yield return new WaitForSecondsRealtime(delay);
            else
                yield return new WaitForSeconds(delay);

            if (action != null)
            action.Invoke();
        }
    }
}
