﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using System.Collections.Generic;

namespace Wild.Freelance.CameraEffectSettings
{
    [CreateAssetMenu(fileName = "CameraEffectSettingsData", menuName = "WildFreelance/CameraEffectSettingsData")]
    public class CameraEffectSettingsData : ScriptableObject
    {
        public List<bool> usedEffects = new List<bool>();

        [ContextMenu("Reset Used Effects in PlayerPrefs")]
        private void ResetUsedEffectsInPlayerPrefs()
        {
            CameraEffectSettingsManager.ResetUsedEffects();
        }
    }
}
