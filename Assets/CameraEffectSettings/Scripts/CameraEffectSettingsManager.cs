﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Wild.Freelance.CameraEffectSettings
{
    public static class CameraEffectSettingsManager
    {
        [Serializable]
        private class UsedEffectsSerializableContainer
        {
            public UsedEffectsSerializableContainer() { }

            public UsedEffectsSerializableContainer(List<bool> usedEffects)
            {
                this.usedEffects = usedEffects;
            }

            public List<bool> usedEffects = new List<bool>();
        }

        static CameraEffectSettingsManager()
        {
            LoadUsedEffects();
            OnUsedEffectsChanged += SaveUsedEffects;
        }

        private const string _usedEffectsKey = "Wild.Freelance.CameraEffectSettings.UsedEffects";

        private static List<bool> _usedEffects = new List<bool>();
        public static List<bool> UsedEffects { get { return _usedEffects; } private set { _usedEffects = value; } }

        public static void SetUsedEffect(uint id, bool isOn)
        {
            int index = (int)id;
            if(UsedEffects.Count - 1 < index)
            {
                Debug.LogException(new Exception("UsedEffectId должен быть меньше, чем количество usedEffects"));
                return;
            }

            UsedEffects[index] = isOn;
            UsedEffectsChanged();
        }

        public static bool GetUsedEffect(uint id)
        {
            int index = (int)id;
            if (UsedEffects.Count - 1 < index)
            {
                Debug.LogException(new Exception("UsedEffectId должен быть меньше, чем количество usedEffects"));
                return false;
            }

            return UsedEffects[index];
        }

        private static void LoadUsedEffects()
        {
            List<bool> usedEffectsFromResources = new List<bool>(Resources.Load<CameraEffectSettingsData>("WildFreelance/CameraEffectSettingsData").usedEffects);            
            try
            {
                List<bool> usedEffectsFromPlayerPrefs = new List<bool>();
                if (PlayerPrefs.HasKey(_usedEffectsKey))
                {
                    string serialized = PlayerPrefs.GetString(_usedEffectsKey);
                    usedEffectsFromPlayerPrefs = JsonUtility.FromJson<UsedEffectsSerializableContainer>(serialized).usedEffects;
                }
                if (usedEffectsFromPlayerPrefs.Count == usedEffectsFromResources.Count)
                    UsedEffects = usedEffectsFromPlayerPrefs;
                else
                    UsedEffects = usedEffectsFromResources;
            }
            catch
            {
                UsedEffects = usedEffectsFromResources;
            }
        }

        private static void SaveUsedEffects()
        {
            string serialized = JsonUtility.ToJson(new UsedEffectsSerializableContainer(UsedEffects));
            PlayerPrefs.SetString(_usedEffectsKey, serialized);
        }

        public static void ResetUsedEffects()
        {
            PlayerPrefs.DeleteKey(_usedEffectsKey);
        }

        public static event Action OnUsedEffectsChanged;

        private static void UsedEffectsChanged()
        {
            if (OnUsedEffectsChanged != null)
                OnUsedEffectsChanged();
        }
    }
}
