﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.CameraEffectSettings
{
    public class CameraEffectsController : MonoBehaviour
    {
        public List<MonoBehaviour> usedEffects = new List<MonoBehaviour>();

        private void Start()
        {
            SyncUsedEffects();
            CameraEffectSettingsManager.OnUsedEffectsChanged += SyncUsedEffects;
        }

        private void SyncUsedEffects()
        {
            for (int i = 0; i < CameraEffectSettingsManager.UsedEffects.Count; i++)
            {
                if (i > usedEffects.Count - 1)
                {
                    Debug.LogException(new Exception("количество эффектов в CameraEffectSettingsData.usedEffects должно совпадать с количеством эффектов в cameraEffects"));
                    break;
                }

                usedEffects[i].enabled = CameraEffectSettingsManager.UsedEffects[i];
            }

            if(usedEffects.Count > CameraEffectSettingsManager.UsedEffects.Count)
                Debug.LogException(new Exception("количество эффектов в CameraEffectSettingsData.usedEffects должно совпадать с количеством эффектов в cameraEffects"));
        }

        private void OnDestroy()
        {
            CameraEffectSettingsManager.OnUsedEffectsChanged -= SyncUsedEffects;
        }
    }
}
