﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using UnityEngine.UI;

namespace Wild.Freelance.CameraEffectSettings
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleCameraEffectController : MonoBehaviour
    {
        private Toggle _toggle;

        public uint usedEffectId;

        private void Start()
        {
            _toggle = GetComponent<Toggle>();

            _toggle.isOn = CameraEffectSettingsManager.GetUsedEffect(usedEffectId);

            _toggle.onValueChanged.AddListener(OnValueChanged);        
        }

        private void OnValueChanged(bool isOn)
        {
            CameraEffectSettingsManager.SetUsedEffect(usedEffectId, isOn);
        }

        private void OnDestroy()
        {
            _toggle.onValueChanged.RemoveListener(OnValueChanged);
        }
    }
}
