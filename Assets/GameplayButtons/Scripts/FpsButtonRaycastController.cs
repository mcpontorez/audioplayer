﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.GameplayButtons
{
    public class FpsButtonRaycastController : MonoBehaviour
    {
        [Header("BUTTON")]
        public float DistanceToObject = 3f;
        public LayerMask RaycastLayer;
        public KeyCode DownKey = KeyCode.F;
        public string GameObjectName = "GameplayButton";

        [Header("INFO")]
        [SerializeField]
        private GameObject _infoCanvasPrefab;
        private GameObject _infoCanvasController;
        private GameObject InfoCanvas
        {
            get
            {
                if (!_infoCanvasController)
                    _infoCanvasController = Instantiate(_infoCanvasPrefab);
                return _infoCanvasController;
            }
        }

        private void Update()
        {
            RaycastHit hit = new RaycastHit();
            bool isHit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition).origin,
                                 Camera.main.ScreenPointToRay(Input.mousePosition).direction, out hit, DistanceToObject, RaycastLayer);

            if (isHit && hit.collider.name == GameObjectName)
            {
                var button = hit.collider.GetComponent<GameplayButtonController>();
                if (button)
                {
                    InfoCanvas.gameObject.SetActive(true);
                    if (Input.GetKey(DownKey))
                        hit.collider.GetComponent<GameplayButtonController>().OnDown.Invoke();
                }
                else
                    InfoCanvas.gameObject.SetActive(false);
            }
            else
                InfoCanvas.gameObject.SetActive(false);
        }
    }
}
