﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.GameplayButtons
{
    public class Debuger : MonoBehaviour
    {
        public void Log(string message = null)
        {
            if (string.IsNullOrEmpty(message))
                message = "On " + name + " Debuger.Log() called";
            Debug.Log(message);
        }
    }
}
