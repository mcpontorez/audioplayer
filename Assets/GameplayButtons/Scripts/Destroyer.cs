﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.GameplayButtons
{
    public class Destroyer : MonoBehaviour
    {
        public new void Destroy(Object obj)
        {
            Object.Destroy(obj);
        }

        public void DestroyThisGameObject()
        {
            Object.Destroy(gameObject);
        }

        public void DestroyThis()
        {
            Object.Destroy(this);
        }
    }
}
