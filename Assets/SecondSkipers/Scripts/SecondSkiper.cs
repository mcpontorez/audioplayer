﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Wild.Freelance.SecondSkipers
{
    public sealed class SecondSkiper : MonoBehaviour
    {
        private const string _secondSkiperKey = "Wild.Freelance.SecondSkipers.SecondSkiper";

        private static bool IsSecondStarted
        {
            get
            {
                bool value = false;
                bool.TryParse(PlayerPrefs.GetString(_secondSkiperKey, false.ToString()), out value);
                return value;
            }
            set
            {
                if (value == true)
                    PlayerPrefs.SetString(_secondSkiperKey, value.ToString());
                else
                    PlayerPrefs.DeleteKey(_secondSkiperKey);
            }
        }

        public UnityEvent AnyKeyDowned;
        
        private IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();

            if (!IsSecondStarted)
                IsSecondStarted = true;
            else
                while (true)
                {
                    yield return null;
                    if (Input.anyKeyDown)
                    {
                        AnyKeyDowned.Invoke();
                    }
                }
        }

        [ContextMenu("Reset in PlayerPrefs")]
        private void ResetInPlayerPrefs()
        {
            IsSecondStarted = false;
        }
    }
}
