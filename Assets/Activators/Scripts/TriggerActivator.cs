﻿//// powered by https://vk.com/mcpontorez

using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.Activators
{
    public class TriggerActivator : MonoBehaviour
    {
        public string triggerEnterTag = "Player";

        public List<GameObject> activatedObjects = new List<GameObject>();

        private void Start()
        {
            SetActiveObjects(false);
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            if (!otherCollider.CompareTag(triggerEnterTag))
                return;

            SetActiveObjects(true);

            Destroy(this.gameObject);
        }

        private void SetActiveObjects(bool isActive)
        {
            foreach (var item in activatedObjects)
            {
                if(item)
                    item.SetActive(isActive);
            }
        }
    }
}