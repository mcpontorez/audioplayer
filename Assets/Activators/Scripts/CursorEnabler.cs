﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Activators
{
    public sealed class CursorEnabler
    {
        public static void EnableCursor()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        public static void DisableCursor()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        public static void SetCursorEnable(bool value)
        {
            if (value)
                EnableCursor();
            else
                DisableCursor();
        }
    }
}
