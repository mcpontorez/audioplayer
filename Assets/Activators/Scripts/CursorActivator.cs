﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.Activators
{
    public class CursorActivator : MonoBehaviour
    {
        private void Start()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
