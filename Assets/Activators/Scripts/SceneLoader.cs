﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wild.Freelance.Activators
{
    public class SceneLoader : MonoBehaviour
    {
        public void LoadScene(int sceneBuildIndex)
        {
            SceneManager.LoadScene(sceneBuildIndex);
        }
    }
}
