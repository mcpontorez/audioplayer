﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Activators
{
    public class AppCloser : MonoBehaviour
    {
        public void CloseApp()
        {
#if UNITY_EDITOR
            Debug.Log("App don't close in editor");
#endif
            Application.Quit();
        }
    }
}
