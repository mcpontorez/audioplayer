﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wild.Freelance.Activators
{
    public class DebugLogger : MonoBehaviour
    {
        public void Log(string message)
        {
            Debug.Log(message);
        }
    }
}
