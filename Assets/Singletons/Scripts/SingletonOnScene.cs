﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Singletons
{
    public abstract class SingletonOnScene<TInstance> : MonoBehaviour where TInstance : SingletonOnScene<TInstance>
    {
        private static TInstance _instance = null;
        public static TInstance Instance
        {
            get
            {
                if (ReferenceEquals(_instance, null) && _instance == null)
                    throw new Exception(typeof(TInstance) + " must be active and enabled in scene");
                return _instance;
            }
        }

        protected static bool TryRegisterIntance(TInstance instance)
        {
            if (_instance != null)
            {
                Destroy(instance.gameObject);
                return false;
            }
            _instance = instance;
            _instance.name = "(Singleton) " + typeof(TInstance);
            DontDestroyOnLoad(_instance);
            return true;
        }
    }
}
