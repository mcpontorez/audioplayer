﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using Wild.Freelance.Localization.UI;

namespace Wild.Freelance.DoorKeys
{
    public class InfoCanvasController : MonoBehaviour
    {
        [SerializeField]
        private TextUILocalizator _infoText;

        public uint InfoPhraseId
        {
            get { return _infoText.PhraseId; }
            set { _infoText.PhraseId = value; }
        }
    }
}
