﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using UnityEngine;

namespace Wild.Freelance.DoorKeys
{
    public class FpsDoorController : MonoBehaviour
    {
        public float distanceToObject = 3f;
        public LayerMask raycastLayer;
        [Header("KEY")]
        public bool isHasKey = false;
        public string keyName = "KeyFromDoor";
        public uint keyVisiblePhraseId = 0;
        public uint keyFoundedPhraseId = 0;
        public float keyInfoDelay = 4f;
        [Header("DOOR")]
        public string doorName = "ClosedDoor";        
        public KeyCode useKey = KeyCode.F;     
        public uint doorClosedPhraseId = 0;
        public uint doorOpeningPhraseId = 0;
        [Header("INFO")]
        [SerializeField]
        private InfoCanvasController _infoCanvasControllerPrefab;
        private InfoCanvasController _infoCanvasController;
        public InfoCanvasController InfoCanvasController
        {
            get
            {
                if (!_infoCanvasController)
                    _infoCanvasController = Instantiate(_infoCanvasControllerPrefab);
                return _infoCanvasController;
            }
        }

        private void Update()
        {
            RaycastHit hit = new RaycastHit();
            bool isHit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition).origin,
                                 Camera.main.ScreenPointToRay(Input.mousePosition).direction, out hit, distanceToObject, raycastLayer);
            if (isHit)
            {
                if (hit.collider.name == doorName)
                {
                    InfoCanvasController.gameObject.SetActive(true);
                    if (isHasKey)
                    {
                        InfoCanvasController.InfoPhraseId = doorOpeningPhraseId;
                        if (Input.GetKeyDown(useKey))
                        {
                            DoorController door = hit.collider.GetComponent<DoorController>();
                            door.Open();

                            Destroy(this);
                            Destroy(InfoCanvasController.gameObject);
                            Destroy(door);
                        }
                    }
                    else
                        InfoCanvasController.InfoPhraseId = doorClosedPhraseId;
                }
                else if (hit.collider.name == keyName)
                {
                    InfoCanvasController.InfoPhraseId = keyVisiblePhraseId;
                    InfoCanvasController.gameObject.SetActive(true);

                    if (Input.GetKeyDown(useKey))
                    {
                        Destroy(hit.collider.gameObject);
                        isHasKey = true;
                        InfoCanvasController.InfoPhraseId = keyFoundedPhraseId;
                        StartCoroutine(HidingInfo());
                    }
                }
            }
            else
            {
                if(InfoCanvasController.InfoPhraseId != keyFoundedPhraseId)
                    InfoCanvasController.gameObject.SetActive(false);
            }    
        }

        private IEnumerator HidingInfo()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(keyInfoDelay);
            InfoCanvasController.gameObject.SetActive(false);
        }
    }
}
