﻿//// powered by https://vk.com/mcpontorez

using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.DoorKeys
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]
    public class DoorController : MonoBehaviour
    {
        private AudioSource _audioSource;
        private Animator _animator;

        public AudioClip openingAudioClip;
        public string openingTrigger = "OpeningTrigger";

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _animator = GetComponent<Animator>();
        }

        public void Open()
        {
            _animator.SetTrigger(openingTrigger);
            _audioSource.PlayOneShot(openingAudioClip);
        }
    }
}
