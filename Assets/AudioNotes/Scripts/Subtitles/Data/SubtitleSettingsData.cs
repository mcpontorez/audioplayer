﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using Wild.Freelance.AudioNotes.Subtitles.Management;

namespace Wild.Freelance.AudioNotes.Subtitles.Data
{
    [CreateAssetMenu(fileName = "SubtitleSettingsData", menuName = "WildFreelance/SubtitleSettingsData")]
    public class SubtitleSettingsData : ScriptableObject
    {
        public bool isOn;

        [ContextMenu("Reset IsOn in PlayerPrefs")]
        private void ResetIsOnInPlayerPrefs()
        {
            SubtitleSettingsManager.ResetIsOn();
        }
    }
}
