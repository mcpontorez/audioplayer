﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using Wild.Freelance.Localization.Data;

namespace Wild.Freelance.AudioNotes.Subtitles.Data
{
    [Serializable]
    public class Subtitle : Phrase
    {
        [Range(0.1f,60f)]
        public float delay = 2f;
    }
}
