﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.AudioNotes.Subtitles.Management;

namespace Wild.Freelance.AudioNotes.Subtitles.Controllers
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleSubtitleController : MonoBehaviour
    {
        private Toggle _toggle;

        private void OnEnable()
        {
            _toggle = GetComponent<Toggle>();

            _toggle.isOn = SubtitleSettingsManager.IsOn;
            _toggle.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnValueChanged(bool isOn)
        {
            SubtitleSettingsManager.IsOn = isOn;
        }

        private void OnDisable()
        {
            _toggle.onValueChanged.RemoveListener(OnValueChanged);
        }
    }
}
