﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.AudioNotes.Subtitles.Data;
using Wild.Freelance.AudioNotes.Subtitles.Management;
using Wild.Freelance.Localization.Management;

namespace Wild.Freelance.AudioNotes.Subtitles.Controllers
{
    public class SubtitleCanvasController : MonoBehaviour
    {
        [SerializeField]
        private GameObject _subtitlePanel;

        [SerializeField]
        private Text _subtitleText;

        private void Start()
        {
            _subtitlePanel.SetActive(false);
        }

        public void SetSubtitles(List<Subtitle> subtitles)
        {
            if (_setterSubtitles != null)
                StopCoroutine(_setterSubtitles);
            _setterSubtitles = StartCoroutine(SetterSubtitles(subtitles));
        }

        private Coroutine _setterSubtitles;
        private IEnumerator SetterSubtitles(List<Subtitle> subtitles)
        {
            foreach (var subtitle in subtitles)
            {
                _subtitlePanel.SetActive(SubtitleSettingsManager.IsOn);

                _subtitleText.text = subtitle.GetText(LocalizationManager.Language);
                yield return new WaitForSeconds(subtitle.delay);
            }

            _subtitlePanel.SetActive(false);
            _subtitleText.text = string.Empty;            
        }
    }
}
