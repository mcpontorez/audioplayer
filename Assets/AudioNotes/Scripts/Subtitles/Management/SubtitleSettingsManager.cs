﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using Wild.Freelance.AudioNotes.Subtitles.Data;

namespace Wild.Freelance.AudioNotes.Subtitles.Management
{
    public static class SubtitleSettingsManager
    {
        static SubtitleSettingsManager()
        {
            Load();
            OnIsOnChanged += (isOn) => Save();
        }

        private const string _isOnKey = "Wild.Freelance.SubtitleSettings.IsOn";

        private static bool _isOn = true;
        public static bool IsOn { get { return _isOn; } set { _isOn = value; CallOnIsOnChanged(_isOn); } }

        public static event Action<bool> OnIsOnChanged;
        private static void CallOnIsOnChanged(bool isOn)
        {
            if (OnIsOnChanged != null)
                OnIsOnChanged(isOn);
        }

        private static void Load()
        {
            bool isOnFromResources = Resources.Load<SubtitleSettingsData>("WildFreelance/SubtitleSettingsData").isOn;
            try
            {
                if (PlayerPrefs.HasKey(_isOnKey))
                    _isOn = Convert.ToBoolean(PlayerPrefs.GetString(_isOnKey));
                else
                    _isOn = isOnFromResources;
            }
            catch
            {
                _isOn = isOnFromResources;
            }
        }

        private static void Save()
        {
            PlayerPrefs.SetString(_isOnKey, IsOn.ToString());
        }

        public static void ResetIsOn()
        {
            PlayerPrefs.DeleteKey(_isOnKey);
        }
    }
}
