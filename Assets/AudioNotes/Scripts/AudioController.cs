﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using UnityEngine;
using Wild.Freelance.AudioNotes.Subtitles.Controllers;
using Wild.Freelance.Characters.FirstPerson;

namespace Wild.Freelance.AudioNotes
{
    [RequireComponent(typeof(FirstPersonController))]
    public class AudioController : MonoBehaviour
    {
        [SerializeField]
        private SubtitleCanvasController _subtitleCanvasControllerPrefab;
        public SubtitleCanvasController SubtitleCanvasController { get; private set; }

        public AudioSource audioSource;
        private FirstPersonController _firstPersonController;

        public string audioNotesTag = "AudioNotes";

        private void Awake()
        {
            _firstPersonController = GetComponent<FirstPersonController>();

            SubtitleCanvasController = Instantiate(_subtitleCanvasControllerPrefab);
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (!collider.CompareTag(audioNotesTag))
                return;

            AudioNoteController audioNote = collider.GetComponent<AudioNoteController>();
            if (!audioNote)
                return;

            audioSource.Stop();
            audioSource.PlayOneShot(audioNote.audioClip);

            SubtitleCanvasController.SetSubtitles(audioNote.subtitles);

            if (audioNote.isStopPlayer)
            {
                _firstPersonController.isMoving = false;
                StartCoroutine(ToOnMoving(audioNote.audioClip.length));
            }

            Destroy(audioNote.gameObject);
        }

        private IEnumerator ToOnMoving(float second)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(second);

            _firstPersonController.isMoving = true;
        }
    }
}
