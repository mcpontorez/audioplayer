﻿//// powered by https://vk.com/mcpontorez

using System.Collections.Generic;
using UnityEngine;
using Wild.Freelance.AudioNotes.Subtitles.Data;

namespace Wild.Freelance.AudioNotes
{
    public class AudioNoteController : MonoBehaviour
    {
        public bool isStopPlayer = true;
        public AudioClip audioClip;

        public List<Subtitle> subtitles = new List<Subtitle>();
    }
}
