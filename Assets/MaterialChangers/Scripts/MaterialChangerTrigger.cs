﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.MaterialChangers
{
    public class MaterialChangerTrigger : MonoBehaviour
    {
        public string triggerEnterTag = "Player";

        public Material finalMaterial;

        public List<MeshRenderer> changedMeshRenderers = new List<MeshRenderer>();

        private void OnTriggerEnter(Collider otherCollider)
        {
            if (!otherCollider.CompareTag(triggerEnterTag))
                return;

            foreach (var renderer in changedMeshRenderers)
            {
                if(renderer)
                    renderer.sharedMaterial = new Material(finalMaterial);
            }

            Destroy(this.gameObject);
        }
    }
}