﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourcePauseIgnorer : MonoBehaviour
    {
        private void OnEnable()
        {
            GetComponent<AudioSource>().ignoreListenerPause = true;
        }
    }
}
