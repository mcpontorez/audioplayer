﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.Localization.UI;

namespace Wild.Freelance.Menus
{
    public sealed class SliderController : MonoBehaviour
    {
        [SerializeField]
        private TextUILocalizator _label;
        public TextUILocalizator Label { get { return _label; } }
        [SerializeField]
        private Slider _slider;
        public Slider Slider { get { return _slider; } }
    }
}
