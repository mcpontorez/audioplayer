﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    public sealed class PauseHandler : MonoBehaviour
    {
        [SerializeField]
        private List<Behaviour> _behavioursToDisableWhenPause = new List<Behaviour>();
        [SerializeField]
        private List<GameObject> _gameObjectsToDisableWhenPause = new List<GameObject>();

        private void OnEnable()
        {
            GameStateService.OnIsPauseChanged += SetDisableObjects;
        }

        private void SetDisableObjects(bool value)
        {
            bool enable = !value;
            foreach (var behaviour in _behavioursToDisableWhenPause)
            {
                if (behaviour != null)
                    behaviour.enabled = enable;
            }
            foreach (var gameObject in _gameObjectsToDisableWhenPause)
            {
                if (gameObject != null)
                    gameObject.SetActive(enable);
            }
        }

        private void OnDisable()
        {
            GameStateService.OnIsPauseChanged -= SetDisableObjects;
        }
    }
}
