﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    public sealed class GameStateSceneData : MonoBehaviour
    {
        [SerializeField]
        private SceneTypes _gameStatesSceneTypes;
        
        public SceneTypes GameStatesSceneTypes { get { return _gameStatesSceneTypes; } }
    }
}
