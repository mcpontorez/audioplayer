﻿using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    public sealed class SettingsManager
    {
        private const string _settingsManagerKey = "Wild.Freelance.Menus.SettingsManager";

        private SettingsData _settingsData = new SettingsData();

        public int QualityLevelId
        {
            get { return _settingsData.QualityLevelId; }
            set
            {
                QualitySettings.SetQualityLevel(value, true);
                _settingsData.QualityLevelId = value;
            }
        }
        public int ScreenResolutionId
        {
            get { return _settingsData.ScreenResolutionId; }
            set
            {
                Resolution resolution = Screen.resolutions[value];
                if(!resolution.Equals(Screen.currentResolution))
                    Screen.SetResolution(resolution.width, resolution.height, true);
                _settingsData.ScreenResolutionId = value;
            }
        }

        public float Volume
        {
            get { return _settingsData.Volume; }
            set
            {
                _settingsData.Volume = value;
                AudioListener.volume = value;
                SaveSettings();
            }
        }

        public SettingsManager()
        {
            SettingsData settingsData = null;

            if (PlayerPrefs.HasKey(_settingsManagerKey))
            {
                try
                {
                    settingsData = JsonUtility.FromJson<SettingsData>(PlayerPrefs.GetString(_settingsManagerKey));
                }
                catch (Exception) { }
            }

            if (settingsData == null)
                settingsData = new SettingsData();

            QualityLevelId = GetTrueId(settingsData.QualityLevelId, QualitySettings.names.Length);

            ScreenResolutionId = GetTrueId(settingsData.ScreenResolutionId, Screen.resolutions.Length);

            Volume = settingsData.Volume;
        }

        private int GetTrueId(int id, int arrayLeght)
        {
            if (id == -1 || id >= arrayLeght)
                return arrayLeght - 1;
            return id;
        }

        public void SaveSettings()
        {
            PlayerPrefs.SetString(_settingsManagerKey, JsonUtility.ToJson(_settingsData));
        }

        public static void ResetSettingsInPlayerPrefs()
        {
            PlayerPrefs.DeleteKey(_settingsManagerKey);
        }
    }
}
