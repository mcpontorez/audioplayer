﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.Menus
{
    [CreateAssetMenu(fileName = "SettingsServiceData", menuName = "WildFreelance/SettingsServiceData")]
    public class SettingsServiceData : ScriptableObject
    {
        [SerializeField]
        private SettingsCanvasController _settingsCanvasControllerPrefab;
        public SettingsCanvasController SettingsCanvasControllerPrefab { get { return _settingsCanvasControllerPrefab; } }

        [ContextMenu("Reset settings in PlayerPrefs")]
        private void ResetSettingsInPlayerPrefs()
        {
            SettingsManager.ResetSettingsInPlayerPrefs();
        }
    }
}
