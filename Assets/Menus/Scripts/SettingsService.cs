﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    public static class SettingsService
    {
        public static SettingsManager SettingsManager { get; private set; }

        private static SettingsCanvasController SettingsCanvasController { get; set; }

        public static void ShowSettingsCanvas(Action onNextHide = null)
        {
            SettingsCanvasController.gameObject.SetActive(true);
            SettingsCanvasController.OnNextHide += onNextHide;
        }
        public static void HideSettingsCanvas()
        {
            SettingsCanvasController.gameObject.SetActive(false);
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void Init()
        {
            SettingsManager = new SettingsManager();

            SettingsCanvasController
                = UnityEngine.Object.Instantiate(Resources.Load<SettingsServiceData>("WildFreelance/SettingsServiceData")
                    .SettingsCanvasControllerPrefab);
            UnityEngine.Object.DontDestroyOnLoad(SettingsCanvasController);

            HideSettingsCanvas();
        }
    }
}
