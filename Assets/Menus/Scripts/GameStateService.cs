﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Wild.Freelance.Activators;
using Wild.Freelance.GameLogics;

namespace Wild.Freelance.Menus
{
    public static class GameStateService
    {
        private static GameStateServiceData Data { get; set; }

        private static PauseCanvasController PauseCanvasController { get; set; }

        public static bool IsPause
        {
            get { return GameState == GameStates.Pause; }
        }

        private static GameStates _gameState = GameStates.None;
        public static GameStates GameState
        {
            get { return _gameState; }
            set
            {
                _gameState = value;

                Time.timeScale = IsPause ? 0f : 1f;
                AudioListener.pause = IsPause;

                PauseCanvasController.gameObject.SetActive(IsPause);

                if (OnIsPauseChanged != null)
                    OnIsPauseChanged.Invoke(IsPause);
            }
        }

        public static event Action<bool> OnIsPauseChanged;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void Init()
        {
            Data = Resources.Load<GameStateServiceData>("WildFreelance/GameStateServiceData");

            PauseCanvasController = UnityEngine.Object.Instantiate(Data.PauseCanvasControllerPrefab);
            UnityEngine.Object.DontDestroyOnLoad(PauseCanvasController);

            InitGameState();

            GameLogicUpdateSystem gameLogicUpdateSystem = GameLogicUpdateSystem.Instance;
            gameLogicUpdateSystem.OnUpdated += OnUpdate;
            gameLogicUpdateSystem.OnLateUpdated += OnLateUpdate;

            SceneManager.sceneLoaded += (s, lsm) => InitGameState();
        }

        private static void InitGameState()
        {
            GameState =
                GetCurrentSceneType() == SceneTypes.Playable ? GameStates.Playing : GameStates.None;
        }

        private static SceneTypes GetCurrentSceneType()
        {
            SceneTypes sceneType = SceneTypes.None;

            GameStateSceneData[] gameStateSceneDatas = UnityEngine.Object.FindObjectsOfType<GameStateSceneData>();
            if (gameStateSceneDatas.Length == 1)
                sceneType = gameStateSceneDatas[0].GameStatesSceneTypes;
            else if (gameStateSceneDatas.Length > 1)
                Debug.LogException(new Exception("GameStateSceneData must have only one or zero instance on scene"));
            
            return sceneType;
        }

        public static void ShowPauseCanvas()
        {
            //если уже задестроен
            if (PauseCanvasController != null && !ReferenceEquals(PauseCanvasController, null))
                PauseCanvasController.gameObject.SetActive(true);
        }

        public static void HidePauseCanvas()
        {
            PauseCanvasController.gameObject.SetActive(false);
        }

        private static void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && GameState == GameStates.Playing)
                    GameState = GameStates.Pause;
        }

        private static void OnLateUpdate()
        {
            CursorEnabler.SetCursorEnable(GameState != GameStates.Playing);
        }
    }
}
