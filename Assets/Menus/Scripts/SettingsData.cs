﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    [Serializable]
    public class SettingsData
    {
        public int QualityLevelId = -1;
        public int ScreenResolutionId = -1;
        public float Volume = 1f;
    }
}
