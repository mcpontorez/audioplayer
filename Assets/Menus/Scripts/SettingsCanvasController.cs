﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace Wild.Freelance.Menus
{
    public sealed class SettingsCanvasController : MonoBehaviour
    {
        private SettingsManager SettingsManager { get { return SettingsService.SettingsManager; } }

        [SerializeField]
        private Button _applyButton;
        [SerializeField]
        private Button _backButton;

        [Header("")]
        [SerializeField]
        private SliderController _volumeSliderController;
        [SerializeField]
        private SliderController _resolutionSliderController;

        [Header("")]
        [SerializeField]
        private SliderController _qualitySliderController;
        [SerializeField]
        private uint[] _qualityLevelIdPhraseIdPairs = new uint[6];

        public event Action OnNextHide;

        private void SetNeedApply(bool value = true)
        {
            _applyButton.interactable = value;
        }

        private void Awake()
        {
            _volumeSliderController.Slider.wholeNumbers = false;
            _volumeSliderController.Slider.minValue = 0f;
            _volumeSliderController.Slider.maxValue = 1f;
            _volumeSliderController.Slider.onValueChanged.AddListener(v =>
            {
                SettingsManager.Volume = _volumeSliderController.Slider.value;
                _volumeSliderController.Label.AdditionalString = " " + (v * 100f).ToString("0", CultureInfo.InvariantCulture);
            });            

            _resolutionSliderController.Slider.wholeNumbers = true;
            _resolutionSliderController.Slider.minValue = 0;
            _resolutionSliderController.Slider.maxValue = Screen.resolutions.Length - 1;
            _resolutionSliderController.Slider.onValueChanged.AddListener(v =>
            {
                SetNeedApply();
                _resolutionSliderController.Label.AdditionalString = " " + Screen.resolutions[Mathf.RoundToInt(v)];
            });

            _qualitySliderController.Slider.wholeNumbers = true;
            _qualitySliderController.Slider.minValue = 0;
            _qualitySliderController.Slider.maxValue = QualitySettings.names.Length - 1;
            _qualitySliderController.Slider.onValueChanged.AddListener(v =>
            {
                SetNeedApply();
                int value = Mathf.RoundToInt(v);
                uint phraseId = 0;
                if (value >= _qualityLevelIdPhraseIdPairs.Length)
                    Debug.LogException(new Exception("PhraseId for QualityLevelId not found because QualityLevelIdPhraseIdPairs leght less than " + value));
                else
                    phraseId = _qualityLevelIdPhraseIdPairs[value];
                _qualitySliderController.Label.PhraseId = phraseId;
            });
        }

        private void OnEnable()
        {
            _backButton.onClick.AddListener(SettingsService.HideSettingsCanvas);

            _volumeSliderController.Slider.value = SettingsManager.Volume;
            _resolutionSliderController.Slider.value = SettingsManager.ScreenResolutionId;
            _qualitySliderController.Slider.value = SettingsManager.QualityLevelId;

            SetNeedApply(false);
            _applyButton.onClick.AddListener(ApplyAndSaveSettings);
        }

        private void ApplyAndSaveSettings()
        {
            SetNeedApply(false);

            SettingsManager.ScreenResolutionId = Mathf.RoundToInt(_resolutionSliderController.Slider.value);
            SettingsManager.QualityLevelId = Mathf.RoundToInt(_qualitySliderController.Slider.value);

            SettingsManager.SaveSettings();
        }

        private void OnDisable()
        {
            _applyButton.onClick.RemoveListener(ApplyAndSaveSettings);
            _backButton.onClick.RemoveListener(SettingsService.HideSettingsCanvas);

            if (OnNextHide != null)
            {
                OnNextHide.Invoke();
                OnNextHide = null;
            }
        }
    }
}
