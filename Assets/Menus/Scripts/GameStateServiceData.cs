﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    [CreateAssetMenu(fileName = "GameStateServiceData", menuName = "WildFreelance/GameStateServiceData")]
    public class GameStateServiceData : ScriptableObject
    {
        [SerializeField]
        private PauseCanvasController _pauseCanvasControllerPrefab;
        public PauseCanvasController PauseCanvasControllerPrefab { get { return _pauseCanvasControllerPrefab; } }
    }
}
