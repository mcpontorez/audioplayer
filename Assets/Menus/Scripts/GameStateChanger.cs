﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{ 
    public sealed class GameStateChanger : MonoBehaviour
    {
        public void ChangeGameSateToPlaying()
        {
            ChangeGameSate(GameStates.Playing);
        }

        public void ChangeGameSate(GameStates gameState)
        {
            GameStateService.GameState = gameState;
        }
    }
}
