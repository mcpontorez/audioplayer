﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;

namespace Wild.Freelance.Menus
{
    public sealed class SettingCanvasShower : MonoBehaviour
    {
        public void HideShowSettingCanvas()
        {
            gameObject.SetActive(false);
            SettingsService.ShowSettingsCanvas(() => gameObject.SetActive(true));
        }
    }
}
