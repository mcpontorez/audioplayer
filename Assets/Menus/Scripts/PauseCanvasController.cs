﻿//// powered by https://vk.com/mcpontorez

using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Wild.Freelance.Menus
{
    public sealed class PauseCanvasController : MonoBehaviour
    {
        [SerializeField]
        private Button _resumeGameButton;

        [SerializeField]
        private Button _mainMenuButton;

        [SerializeField]
        private Button _settingsMenuButton;

        private void Start()
        {
            _resumeGameButton.onClick.AddListener(ResumeGame);
            _mainMenuButton.onClick.AddListener(OpenMainMenu);
            _settingsMenuButton.onClick.AddListener(OpenSettings);
        }

        private void OpenSettings()
        {
            GameStateService.HidePauseCanvas();
            SettingsService.ShowSettingsCanvas(GameStateService.ShowPauseCanvas);
        }

        private void ResumeGame()
        {
            GameStateService.GameState = GameStates.Playing;
        }

        private void OpenMainMenu()
        {
            GameStateService.HidePauseCanvas();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                ResumeGame();
        }

        private void OnDestroy()
        {
            _resumeGameButton.onClick.RemoveListener(ResumeGame);
            _mainMenuButton.onClick.AddListener(OpenMainMenu);
            _settingsMenuButton.onClick.RemoveListener(OpenSettings);
        }
    }
}
