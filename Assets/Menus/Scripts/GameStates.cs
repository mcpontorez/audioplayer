﻿
namespace Wild.Freelance.Menus
{
    public enum GameStates
    {
        None = 0,
        Playing = 1,
        Pause = 2
    }
}
