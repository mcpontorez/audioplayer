﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.DoorShredders
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(AudioSource))]
    public class DoorController : MonoBehaviour
    {
        private AudioSource _audioSource;

        private Animator _animator;
        public string closureTrigger = "ClosureTrigger";
        public AudioClip closureAudioClip;
        private bool _isClose = false;
        public string breakingOutTrigger = "BreakingOutTrigger";

        public List<AudioClip> hitAudioClips = new List<AudioClip>();
        
        public string doorShredder = "DoorShredder";
        [Tooltip("Only positive values")]
        public Vector3 collisionImpulse = Vector3.one;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _animator = GetComponent<Animator>();
        }

        public void Close()
        {
            if (_isClose)
                return;
            _animator.SetTrigger(closureTrigger);
            _audioSource.PlayOneShot(closureAudioClip);
            _isClose = true;
        }

        /// <summary>
        /// Удар по двери
        /// </summary>
        /// <returns>разрушена ли дверь</returns>
        private void SetHit()
        {
            if (!_isClose)
                return;

            if (hitAudioClips.Count > 1)
            {
                _audioSource.PlayOneShot(hitAudioClips[0]);
                hitAudioClips.RemoveAt(0);
            }
            else
            {
                _audioSource.PlayOneShot(hitAudioClips[0]);
                _animator.SetTrigger(breakingOutTrigger);
                Destroy(this);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name != doorShredder)
                return;

            for (int i = 0; i < 3; i++)
            {
                if (Mathf.Abs(collision.impulse[i]) > Mathf.Abs(collisionImpulse[i]))
                {
                    SetHit();
                    break;
                }
            }
        }
    }
}
