﻿//// powered by https://vk.com/mcpontorez

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.DoorShredders
{
    public class ClosureDoorTriggerController : MonoBehaviour
    {
        public DoorController door;
        public string triggerEnterTag = "Player";

        private void OnTriggerEnter(Collider otherCollider)
        {
            if (otherCollider.tag != triggerEnterTag)
                return;

            door.Close();
            Destroy(this);
        }
    }
}
