﻿//Creative Commons Attribution Share Alike - wiki.unity3d.com. Modified a fix for Unity5 and added funcitonal distance.

//This script allows you to drag any rigidbody object and also throw it with force.

//var player : GameObject;
var normalCollisionCount = 1;
var spring = 50.0;
var damper = 5.0;
var drag = 10.0;
var angularDrag = 5.0;
var distance = 0.2;
var throwForce = 500;
var throwRange = 1000;
var speed = 0.2;
var attachToCenterOfMass = false;
 
private var springJoint : SpringJoint;
private var objPos : SpringJoint;
 
function Update ()
{
	if (!Input.GetMouseButtonDown (0))
		return;
 
	var mainCamera = FindCamera();
 
	var hit : RaycastHit;
	if (!Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition),  hit, 2))
		return;
	if (!hit.rigidbody || hit.rigidbody.isKinematic)
		return;

	if (!springJoint)
	{
                var go = new GameObject("Rigidbody dragger");
                body = go.AddComponent.<Rigidbody>();
                springJoint = go.AddComponent.<SpringJoint>();
                body.isKinematic = true;
	}
 
	springJoint.transform.position = hit.point;
	if (attachToCenterOfMass)
	{
		var anchor = transform.TransformDirection(hit.rigidbody.centerOfMass) + hit.rigidbody.transform.position;
		anchor = springJoint.transform.InverseTransformPoint(anchor);
		springJoint.anchor = anchor;
	}
	else
	{
		springJoint.anchor = Vector3.zero;
	}
 
	springJoint.spring = spring;
	springJoint.damper = damper;
	springJoint.maxDistance = distance;
	springJoint.connectedBody = hit.rigidbody;
 
	StartCoroutine ("DragObject", hit.distance);
}
 
function DragObject (distance : float)
{
	var oldDrag = springJoint.connectedBody.drag;
	var oldAngularDrag = springJoint.connectedBody.angularDrag;
	springJoint.connectedBody.drag = drag;
	springJoint.connectedBody.angularDrag = angularDrag;
	var mainCamera = FindCamera();
	grabbed = springJoint.connectedBody.transform;
		                                
	oldYRotCam = mainCamera.transform.eulerAngles.y;
	oldYRotObj = springJoint.connectedBody.transform.eulerAngles.y;
	oldYRotComp = oldYRotCam - oldYRotObj;
	 var objAnchor = springJoint.connectedBody.transform.InverseTransformPoint(springJoint.transform.position);
	while (Input.GetMouseButton (0))
	{
	        		newYRotCam = mainCamera.transform.eulerAngles.y;
        		newYRot = newYRotCam - oldYRotComp;
                var ray = mainCamera.ScreenPointToRay (Input.mousePosition);
                springJoint.transform.position = ray.GetPoint(distance);
                //Physics.IgnoreCollision(player.GetComponent.<Collider>(), grabbed.GetComponent.<Collider>());
                yield;

                 var objPos = springJoint.connectedBody.transform.TransformPoint(objAnchor);
                 var springPos = springJoint.transform.TransformPoint(springJoint.anchor);
                 var currentDist = Vector3.Distance( springPos, objPos );
                 if (currentDist > 2)
                 {
				held = 0;
				springJoint.connectedBody.drag = oldDrag;
				springJoint.connectedBody.angularDrag = oldAngularDrag;
				springJoint.connectedBody = null;
				StopCoroutine ("DragObject");
				//yield;
				}
				//yield;
              if (distance > 5)
			{
				distance = 1.5;
			}
		
		if (distance < 2)
			{
				distance = 1.5;
			}
		
	/*	if (Input.GetAxis("Mouse ScrollWheel") > 0 && distance <= 5)
			{
				distance += speed;
			}
		
		if (Input.GetAxis("Mouse ScrollWheel") < 0 && distance >= 2)
			{
				distance -= speed;
			} 

		/*if(player.GetComponent.<Collider>() && distance <= 5) 
			{
				held = 0;
				springJoint.connectedBody.drag = oldDrag;
				springJoint.connectedBody.angularDrag = oldAngularDrag;
				springJoint.connectedBody = null;
				StopCoroutine ("DragObject");
			}

			if(Vector3.Distance(player.transform.position, springJoint.transform.position) >= 5) 
			{
				held = 0;
				springJoint.connectedBody.drag = oldDrag;
				springJoint.connectedBody.angularDrag = oldAngularDrag;
				springJoint.connectedBody = null;
				StopCoroutine ("DragObject");
			} */

		if (Input.GetKeyDown(KeyCode.LeftShift))
			{
				held = 0;
				springJoint.connectedBody.drag = oldDrag;
				springJoint.connectedBody.angularDrag = oldAngularDrag;
				springJoint.connectedBody = null;
				StopCoroutine ("DragObject");
			}
 
		if (Input.GetMouseButton (1)){
				held = 0;
				springJoint.connectedBody.AddExplosionForce(throwForce,mainCamera.transform.position,throwRange);
				springJoint.connectedBody.drag = oldDrag;
				springJoint.connectedBody.angularDrag = oldAngularDrag;
				springJoint.connectedBody = null;
				StopCoroutine ("DragObject");
				yield;

		}
	}
	if (springJoint.connectedBody)
	{
        		//Physics.IgnoreCollision(player.GetComponent.<Collider>(), grabbed.GetComponent.<Collider>(), false);
        		grabbed = null;
                springJoint.connectedBody.drag = oldDrag;
                springJoint.connectedBody.angularDrag = oldAngularDrag;
                springJoint.connectedBody = null;
	}
}
 
function FindCamera ()
{
	if (GetComponent.<Camera>())
		return GetComponent.<Camera>();
	else
		return Camera.main;
}