﻿//// powered by https://vk.com/mcpontorez

using UnityEngine;

namespace Wild.Freelance.RigidBodyAudios
{
    [RequireComponent(typeof(HingeJoint))]
    [RequireComponent(typeof(AudioSource))]
    public class DoorAudioController : MonoBehaviour
    {
        private Rigidbody _rigidBody;
        private HingeJoint _hingeJoint;
        private AudioSource _audioSource;

        [SerializeField]
        private Vector3 _testMaxAngularVelocity = Vector2.zero;
        public Vector3 TestMaxAngularVelocity {
            get { return _testMaxAngularVelocity; }
            set
            {
                if (Time.timeSinceLevelLoad < 1f)
                    return;

                for (int i = 0; i < 3; i++)
                {
                    if (Mathf.Abs(value[i]) > Mathf.Abs(_testMaxAngularVelocity[i]))
                        _testMaxAngularVelocity[i] = Mathf.Abs(value[i]);
                }
            }
        }
        [SerializeField]
        private Vector3 _maxAngularVelocityForVolume = Vector3.one;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _hingeJoint = GetComponent<HingeJoint>();
            _rigidBody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            Vector3 axis = _hingeJoint.axis;
            for (int i = 0; i < 3; i++)
            {
                if (!Mathf.Approximately(axis[i], 0f))
                {
                    if (!Mathf.Approximately(_rigidBody.angularVelocity[i], 0f))
                    {
                        if (!_audioSource.isPlaying)
                            _audioSource.Play();
                        float volume = Mathf.Abs(_rigidBody.angularVelocity[i] / _maxAngularVelocityForVolume[i]);
                        _audioSource.volume = volume < 1f ? volume : 1f; 
                    }
                    else
                    {
                        if (_audioSource.isPlaying)
                            _audioSource.Pause();
                    }
                }
            }

            TestMaxAngularVelocity = _rigidBody.angularVelocity;
        }
    }
}
