﻿//// powered by https://vk.com/mcpontorez

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Wild.Freelance.RigidBodyAudios
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(AudioSource))]
    public class CollisionAudioController : MonoBehaviour
    {
        private AudioSource _audioSource;
        [SerializeField]
        private AudioClip[] _audioClips = new AudioClip[1];

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();

            if(_audioClips.Length < 1 || _audioClips.Any(a => a == null))
                Debug.LogException(new Exception(name + " in CollisionAudioController must have at least 1 cell and not have null or none cell in AudioClips"));
        }

        private void OnCollisionEnter(Collision collision)
        {
            _audioSource.PlayOneShot(_audioClips[UnityEngine.Random.Range(0, _audioClips.Length)]);
        }
    }
}
